/*
 * 请告诉我所有的订单（`order`）中每一种 `status` 的订单的总金额到底是多少。注意是总金额哦。输出
 * 应当包含如下的信息：
 *
 * +─────────+─────────────+
 * | status  | totalPrice  |
 * +─────────+─────────────+
 *
 * 输出应当根据 `status` 进行排序。
 */
SELECT o.status, sum(d.quantityOrdered * d.priceEach) As totalPrice
FROM orders o LEFT JOIN  orderdetails d ON d.orderNumber = o.orderNumber
GROUP BY o.status ORDER BY o.status;

# SELECT o.orderNumber, sum(d.quantityOrdered * d.priceEach) As totalPrice, count(o.`orderNumber`) as detailsCount
# from orderdetails d join orders o on d.`orderNumber` = o.`orderNumber`
# where o.status = 'Cancelled' group by o.orderNumber;