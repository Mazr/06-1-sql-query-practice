/*
 * 请告诉我所有订单（order）中的订单明细的最大数目、最小数目和平均数目。结果应当包含三列：
 *
 * +────────────────────+────────────────────+────────────────────+
 * | minOrderItemCount  | maxOrderItemCount  | avgOrderItemCount  |
 * +────────────────────+────────────────────+────────────────────+
 */
SELECT min(c.orderItemCount) AS minOrderItemCount ,max(c.orderItemCount) AS maxOrderItemCount,ROUND(avg(c.orderItemCount)) AS avgOrderItemCount
FROM (
     SELECT count(o.orderNumber) AS orderItemCount
     FROM orders o LEFT JOIN orderdetails d ON o.orderNumber = d.orderNumber
     GROUP BY o.orderNumber
) AS c