/*
 * 请告诉我总金额大于 60000 的每一个订单（`order`）编号及其总金额。查询结果应当包含如下信息：
 * 
 * +──────────────+─────────────+
 * | orderNumber  | totalPrice  |
 * +──────────────+─────────────+
 *
 * 其结果应当以 `orderNumber` 排序。
 */
SELECT o.orderNumber,sum(d.quantityOrdered * d.priceEach) As totalPrice
FROM orders o LEFT JOIN orderdetails d ON d.orderNumber = o.orderNumber
GROUP BY o.orderNUmber
HAVING totalPrice >60000
ORDER BY o.orderNUmber;